from django.contrib import admin
from django.urls import path, include, register_converter

from utils.converters import UsernameConverter, UUIDConverter

# 注册转换器
register_converter(UsernameConverter, 'uname')
register_converter(UUIDConverter, 'uuid')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('apps.users.urls')),
    path('', include('apps.verifications.urls')),
    path('', include('apps.areass.urls'))


]

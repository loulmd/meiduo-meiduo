# celery启动文件
import os

from celery import Celery

# 启动celery服务前，要设置django环境
# 虽然celery跟django没有关系，但是是在django中运行，所以需要配置django环境
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'meiduo_mall.settings')


# 创建celery实例
celery_app = Celery('celery_tasks')
# 加载celery配置
celery_app.config_from_object('celery_tasks.config')
# 自动注册celery任务, 对自动去找 sms、email下面的tasks.py文件，所以文件名一定要以tasks.py命名
celery_app.autodiscover_tasks(['celery_tasks.sms','celery_tasks.email'])

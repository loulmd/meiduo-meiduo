from django.core.mail import send_mail

from celery_tasks.main import celery_app
from utils.smsutils import SmsUtil


@celery_app.task(name='send_active_email')
def send_active_email(email_addr, message):
    print('异步邮件1')
    subject = '美多商城邮箱验证'
    message = message
    from_email = '<2601303582@qq.com>'
    recipient_list = [email_addr]
    send_mail(subject=subject, message='', from_email=from_email, recipient_list=recipient_list,html_message=message)
    print('异步邮件2')

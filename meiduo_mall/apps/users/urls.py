
from django.contrib import admin
from django.urls import path, include

from apps.users.views import UsernameCountView, RegisterView, LoginView, LogoutView, UserInfoView, SaveEmailView, EmailVerifyView

urlpatterns = [

    # 判断 用户名是否存
    path('usernames/<uname:username>/count/', UsernameCountView.as_view()),

    # 注册的视图
    path('register/', RegisterView.as_view()),

    # 登录
    path('login/', LoginView.as_view()),

    # 退出
    path('logout/', LogoutView.as_view()),

    # 用户中心获取用户数据的请求
    path('info/', UserInfoView.as_view()),

    # 添加邮箱
    path('emails/', SaveEmailView.as_view()),

    # 激活邮箱
    path('emails/verification/', EmailVerifyView.as_view()),
]

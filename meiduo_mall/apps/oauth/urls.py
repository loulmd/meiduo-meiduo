from django.urls import path

from apps.oauth.views import WeiboAuthURLView, WeiboAuthUserView

urlpatterns = [
    #获取登录页面的视图
    path('qq/authorization/', WeiboAuthURLView.as_view()),
    # oauth_callback/?code=7b56778e56ad2df6132e8bcd72c4e705
    # 登录用户验证的视图
    path('oauth_callback/', WeiboAuthUserView.as_view()),

]

import json
import http

from django.http import JsonResponse

from django.shortcuts import render
from django.core.cache import cache
# Create your views here.

from django.views import View

from apps.areass.models import Area, Addresses
from utils.views import LoginRequiredJSONMixin


class AreasView(View):
    def get(self, request):
        province_list = cache.get('provinces')
        if not province_list:
            print('省市区没有缓存')
            # 1  去数据库查询所有的省份数据
            try:
                areas = Area.objects.filter(parent=None)
            except Exception as e:
                print(e)
                return JsonResponse({'code': 400, 'errmsg': "获取省份失败，网络异常"})

            province_list = []

            for a in areas:
                province_list.append({"id": a.id, "name": a.name})

            cache.set('provinces', province_list, 3600 * 24 * 30)

        else:
            print('使用了缓存')

        # 2  返回
        return JsonResponse({'code': 0, 'province_list': province_list})


class SubAreasView(View):
    def get(self, request, area_id):
        # 1 获取路径中的上一级的地区的id
        # 正则校验area_id

        # 去缓存找
        subs = cache.get('cities_%s' % area_id)

        if not subs:

            print('市区没有缓存')
            # 2 根据上一级的id获取下一级的数据(数据库操作)
            try:
                subareas = Area.objects.filter(parent_id=area_id)
            except Exception as e:
                print(e)
                return JsonResponse({'code': 400, 'errmsg': "获取市区失败，网络异常"})

            # 3 把下一级数据 做拼接
            subs = []
            for a in subareas:
                subs.append({'id': a.id, 'name': a.name})

            # 把数据存到缓存
            cache.set('cities_%s' % area_id, subs, 3600 * 24 * 30)

        # data.sub_data.subs

        # 4 返回给前端
        return JsonResponse({'code': 0, 'sub_data': {'subs': subs}})


class AddressView(LoginRequiredJSONMixin, View):
    def post(self, request):
        body = request.body
        data_dict = json.loads(body)
        receiver = data_dict.get('receiver')
        province_id = data_dict.get('province_id')
        city_id = data_dict.get('city_id')
        district_id = data_dict.get('district_id')
        place = data_dict.get('place')
        mobile = data_dict.get('mobile')
        tel = data_dict.get('tel')
        email = data_dict.get('email')

        if not all([receiver, province_id, city_id, district_id, place, mobile]):
            return JsonResponse({'code': 400, 'errmsg': "数据不全"})

        try:
            address = Addresses.objects.create(user=request.user,
                                             title=receiver,
                                             receiver=receiver,
                                             province_id=province_id,
                                             city_id=city_id,
                                             district_id=district_id,
                                             place=place,
                                             mobile=mobile,
                                             tel=tel,
                                             email=email)

            request.user.default_address = address.id
            request.user.save()
        except Exception as e:
            print(e)
            return JsonResponse({'code': 505, 'errmsg': "保存失败"})

        address_dict = {
            "id": address.id,
            "title": address.title,
            "receiver": address.receiver,
            "province": address.province.name,
            "city": address.city.name,
            "district": address.district.name,
            "place": address.place,
            "mobile": address.mobile,
            "tel": address.tel,
            "email": address.email
        }

        return JsonResponse({'code': 0, 'errmsg': "ok", 'address': address_dict})

    def get(self, request):

        user = request.user

        try:
            addresses = user.addresses.all()

            address_dict_list = []

            for address in addresses:
                address_dict = {
                    "id": address.id,
                    "title": address.title,
                    "receiver": address.receiver,
                    "province": address.province.name,
                    "city": address.city.name,
                    "district": address.district.name,
                    "place": address.place,
                    "mobile": address.mobile,
                    "tel": address.tel,
                    "email": address.email
                }

                address_dict_list.insert(0, address_dict)
        except Exception as e:
            print(e)
            return JsonResponse({'code': 501, 'errmsg': '查询失败'})

        return JsonResponse({'code': 0, 'errmsg': 'ok', 'addresses': address_dict_list,
                             'default_address_id': request.user.default_address})



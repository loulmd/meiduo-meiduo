from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse


# 前后端分离，用户如果没有登录，返回Json数据，而不是后端重定项跳转页面
# 所以重写LoginRequiredMixin的handle_no_permission的方法，返回json数据
class LoginRequiredJSONMixin(LoginRequiredMixin):
    def handle_no_permission(self):
        return JsonResponse({'code': 400, 'errmsg': '用户未登录'})

import json

from ronglian_sms_sdk import SmsSDK


class SmsUtil:
    __instance = None
    def __new__(cls, *args, **kwargs):
        if not cls.__instance:
            cls.__instance = super().__new__(cls, *args, **kwargs)
            cls.smsSdk = SmsSDK(accId='8a216da87dc23fe1017dc787065901c4',
                                accToken='35afb14932c94b5095eea6bc456f7b24',
                                appId='8aaf07087dc23905017dc788c76501f2')
        return cls.__instance
    def send_message(self,mobile="19825726330",tid="1",code='123456'):
        sendback = self.smsSdk.sendMessage(tid=tid, mobile=mobile, datas=(code,5))
        # print('sendback1',sendback)  # json类型

        # 把返回值json 转为字典
        sendback = json.loads(sendback)
        # print('sendback',sendback)
        if sendback.get("statusCode") == '000000':
            print('发送成功')
        else:
            print('发送失败')

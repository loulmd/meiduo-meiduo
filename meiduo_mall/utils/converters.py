# 自定义路由转换器 匹配用户名
class UsernameConverter:
    regex = '[a-zA-Z0-9_-]{5,20}'

    def to_python(self, value):
        return str(value)

    def to_url(self, value):
        return str(value)


# 自定义路由转换器 匹配手机号
class MobileConverter:
    # 定义匹配手机号的正则表达式
    regex = '1[3-9]\d{9}'

    # to_python 是将匹配到的结果传递到视图内部使用
    def to_python(self, value):
        return str(value)

    def to_url(self, value):
        return str(value)

#   uuid 自定义转换器
class UUIDConverter:
    # uuid: f760ea85-8ad7-4c97-b40b-a94f78b8f058
    regex = '[0-9a-f]{8}(-[0-9a-f]{4}){3}-[0-9a-f]{12}'

    def to_python(self, value):
        return str(value)

    def to_url(self, value):
        return str(value)


